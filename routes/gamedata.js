var interpreter = require('./interpreter');
interpreter.parse();

var userpoint = [];
var userstatus = [];
var results = [];

exports.adduser = function (user) {
        // game started.                                                        
//      user = "hoge";                                                          

    userstatus[user] = { 'started' : true };
    userpoint[user] = { 'point' : 0 };
};

exports.updateuser = function (user, lat, lng) {
//      user = "hoge";                                                          

    console.log(userpoint[user]);
    return interpreter.execute(lat, lng, userpoint[user]);
};

exports.getresult = function () {
    var result = [];

    for (key in userpoint) {
        var obj = {};
        obj["name"] = key;
        var pointobj = userpoint[key];
        var pointvalue = pointobj["point"];
        obj["point"] = pointvalue;
                //console.log(obj);                                             
        result.push(obj);
    }
    return result;
};
